
def fib_number(n):
   if n <= 0:
      return "incorrect, can't be computed"
   else:
      fib = [0, 1]
      if n > 2:
         for i in range(2, n):
            fib.append(fib[i - 1] + fib[i - 2])
   return fib[n - 1]


# input
n = int(input("Enter n: "))
print("{}th fibonacci number is: ".format(n), fib_number(n))